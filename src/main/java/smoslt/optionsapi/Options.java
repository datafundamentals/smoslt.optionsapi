/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.optionsapi module, one of many modules that belongs to smoslt

 smoslt.optionsapi is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.optionsapi is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.optionsapi in the file smoslt.optionsapi/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.optionsapi;

import java.util.ArrayList;
import java.util.List;

public class Options {
	private List<Option> optionList= new ArrayList<Option>();

	public List<Option> getOptionList() {
		return optionList;
	}
	
	public void addOption(Option option){
		optionList.add(option);
	}
	

}
